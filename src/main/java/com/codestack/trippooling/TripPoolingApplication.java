package com.codestack.trippooling;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TripPoolingApplication {

	public static void main(String[] args) {
		System.setProperty("server.servlet.context-path", "/trip");
		SpringApplication.run(TripPoolingApplication.class, args);
	}

}
