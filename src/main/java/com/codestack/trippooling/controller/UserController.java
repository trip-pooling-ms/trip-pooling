package com.codestack.trippooling.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codestack.trippooling.mongo.document.User;
import com.codestack.trippooling.service.UserService;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping(value = "user")
@Slf4j
public class UserController {

	private UserService userService;

	@Autowired
	public UserController(UserService userService) {
		this.userService = userService;

	}

	@PostMapping
	public void saveUser(@RequestBody User user) {
		log.info("Saving user: " + user);
		userService.create(user);
	}

	@GetMapping
	public List<User> findAll() {
		return userService.findAll();
	}

	@GetMapping("/{id}")
	public User find(@PathVariable("id") String id) {
		return userService.findById(id);
	}

	@DeleteMapping("/removeAll")
	public ResponseEntity<String> removeAll() {
		userService.deleteAll();
		return ResponseEntity.status(HttpStatus.OK).body("Removed all documents");
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<String> delete(@PathVariable("id") String id) {
		userService.deleteById(id);
		return ResponseEntity.status(HttpStatus.OK).body("Deleted DrinkOutlet with id " + id);
	}

	@PutMapping("/{id}")
	public ResponseEntity<String> edit(@RequestBody User user, @PathVariable("id") String id) {
		if (userService.edit(user, id)) {
			return ResponseEntity.status(HttpStatus.ACCEPTED).body("Updated " + user);
		}
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body("User with id " + id + " not found");
	}
}
