package com.codestack.trippooling.model;

import com.google.maps.model.Distance;
import com.google.maps.model.Duration;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder(builderMethodName = "hiddenBuilder")
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class RouteDetails {
	public static RouteDetails NOT_FOUND = new RouteDetails(Status.NOT_FOUND);

	public RouteDetails(Status status) {
		this.status = status;
	}

	private String originAddress;
	private String destinationAddress;
	private Distance distance;
	private Duration duration;
	private Status status;

	public static RouteDetailsBuilder builder(String originAddress, String destinationAddress) {
		return hiddenBuilder().originAddress(originAddress).destinationAddress(destinationAddress);
	}

}
