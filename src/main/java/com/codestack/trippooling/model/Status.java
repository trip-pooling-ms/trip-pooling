package com.codestack.trippooling.model;

public enum Status {
	OK, NOT_FOUND, ERROR
}