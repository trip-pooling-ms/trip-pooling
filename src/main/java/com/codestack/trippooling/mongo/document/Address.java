package com.codestack.trippooling.mongo.document;

import org.springframework.data.geo.Point;import org.springframework.data.mongodb.core.index.GeoSpatialIndexType;
import org.springframework.data.mongodb.core.index.GeoSpatialIndexed;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Address {

	private String description;
	@GeoSpatialIndexed(type = GeoSpatialIndexType.GEO_2DSPHERE, name = "address")
	private Point point;

}
