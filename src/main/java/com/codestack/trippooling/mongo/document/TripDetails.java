package com.codestack.trippooling.mongo.document;


import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class TripDetails {

	private Address originAddress;
	private Address destinationAddress;
	private TripSchedule tripSchedule;
}
