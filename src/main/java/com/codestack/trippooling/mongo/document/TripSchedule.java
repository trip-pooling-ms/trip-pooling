package com.codestack.trippooling.mongo.document;

import java.time.DayOfWeek;
import java.time.LocalTime;
import java.util.Set;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class TripSchedule {

	private LocalTime departureTime;
	private LocalTime returnTime;
	private Set<DayOfWeek> dayOfWeeks;
}
