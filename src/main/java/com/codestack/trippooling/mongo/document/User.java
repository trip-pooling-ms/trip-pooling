package com.codestack.trippooling.mongo.document;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Builder;
import lombok.Data;

@Document
@Data
@Builder
public class User {

	@Id
	private String id;
	private String firstName;
	private String lastName;
	private String phoneNumber;
	private TripDetails tripDetails;
}
