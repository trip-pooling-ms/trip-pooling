package com.codestack.trippooling.mongo.repository;

import java.util.List;

import org.springframework.data.geo.Distance;
import org.springframework.data.geo.Point;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.codestack.trippooling.mongo.document.User;

@Repository
public interface UserRepository extends MongoRepository<User, String> {

	public List<User> findByRouteDetailsOriginAddressPointNear(Point point, Distance distance);

}
