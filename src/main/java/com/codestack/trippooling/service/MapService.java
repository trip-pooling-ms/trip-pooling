package com.codestack.trippooling.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.geo.Point;
import org.springframework.stereotype.Service;

import com.codestack.trippooling.model.RouteDetails;
import com.google.maps.DirectionsApi;
import com.google.maps.DistanceMatrixApi;
import com.google.maps.GeoApiContext;
import com.google.maps.errors.ApiException;
import com.google.maps.model.DirectionsResult;
import com.google.maps.model.DistanceMatrix;
import com.google.maps.model.DistanceMatrixElement;
import com.google.maps.model.LatLng;
import com.google.maps.model.TravelMode;
import com.google.maps.model.Unit;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class MapService {

	private GeoApiContext geoApiContext;

	@Autowired
	public MapService(GeoApiContext geoApiContext) {
		this.geoApiContext = geoApiContext;
	}

	public DirectionsResult findDirection(Point originPoint, Point destinationPoint)
			throws ApiException, InterruptedException, IOException {
		DirectionsResult result = DirectionsApi.newRequest(geoApiContext).units(Unit.METRIC)
				.origin(new LatLng(originPoint.getX(), originPoint.getY()))
				.destination(new LatLng(destinationPoint.getX(), destinationPoint.getY())).mode(TravelMode.DRIVING)
				.await();
		log.info("Received result from DirectionsApi: " + result);
		return result;
	}

	public DistanceMatrix findDistanceMatrix(Point originPoint, Point... destinationPoints)
			throws ApiException, InterruptedException, IOException {
		DistanceMatrix result = DistanceMatrixApi.newRequest(geoApiContext)
				.origins(new LatLng(originPoint.getX(), originPoint.getY()))
				.destinations(createLatLngArray(destinationPoints)).mode(TravelMode.DRIVING).await();
		log.info("Received result from DistanceMatrixApi: " + result);

		return result;
	}

	private LatLng[] createLatLngArray(Point... destinationPoints) {
		int length = destinationPoints.length;
		LatLng[] destinations = new LatLng[length];
		for (int i = 0; i < length; i++) {
			destinations[i] = new LatLng(destinationPoints[i].getX(), destinationPoints[i].getY());
		}
		return destinations;
	}

	
	public List<RouteDetails> parseDistanceMatrix(DistanceMatrix distanceMatrix) {
		List<RouteDetails> routeDetailsList = new ArrayList<>();
		DistanceMatrixElement[] elements = distanceMatrix.rows[0].elements;
		System.out.println("elements: " + elements.length);
		for (int i = 0; i < elements.length; i++) {
			RouteDetails routeDetails = RouteDetails
					.builder(distanceMatrix.originAddresses[0], distanceMatrix.destinationAddresses[i])
					.distance(elements[i].distance).duration(elements[i].duration).build();
			routeDetailsList.add(routeDetails);
		}
		return routeDetailsList;
	}
}
