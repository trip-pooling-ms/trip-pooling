package com.codestack.trippooling.service;

import org.springframework.stereotype.Service;

import com.codestack.trippooling.mongo.document.TripDetails;

@Service
public class RouteService {

	public TripDetails createRouteDetails() {
		TripDetails routeDetails = TripDetails.builder().build();
		return routeDetails;
	}

}
