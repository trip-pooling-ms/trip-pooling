package com.codestack.trippooling.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.geo.Distance;
import org.springframework.data.geo.Point;
import org.springframework.stereotype.Service;

import com.codestack.trippooling.mongo.document.User;
import com.codestack.trippooling.mongo.repository.UserRepository;

import org.springframework.data.mongodb.core.MongoTemplate;

@Service
public class UserService {

	private UserRepository userRepository;
	private RouteService routeService;

	@Autowired
	MongoTemplate template;

	@Autowired
	public UserService(UserRepository userRepository, RouteService routeService) {
		this.userRepository = userRepository;
		this.routeService = routeService;
	}

	public void create(User user) {
		userRepository.save(user);
	}

	public User findById(String id) {
		Optional<User> optDrinkOutlet = userRepository.findById(id);
		return optDrinkOutlet.orElse(null);
	}

	public void deleteById(String id) {
		userRepository.deleteById(id);
	}

	public boolean edit(User user, String id) {
		Optional<User> optDrinkOutlet = userRepository.findById(id);
		if (optDrinkOutlet.isPresent()) {
			user.setId(id);
			userRepository.save(user);
			return true;
		}
		return false;
	}

	public void deleteAll() {
		userRepository.deleteAll();
	}

	public List<User> findAll() {
		return userRepository.findAll();
	}

	public List<User> findUsersStartingNear(Point point, Distance distance) {
		return userRepository.findByRouteDetailsOriginAddressPointNear(point, distance);
	}

}
