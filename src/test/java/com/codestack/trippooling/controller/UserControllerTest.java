package com.codestack.trippooling.controller;

import java.time.DayOfWeek;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import static org.mockito.Mockito.*;
import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.codestack.trippooling.TripPoolingApplication;
import com.codestack.trippooling.controller.UserController;
import com.codestack.trippooling.mongo.document.Address;
import com.codestack.trippooling.mongo.document.TripDetails;
import com.codestack.trippooling.mongo.document.TripSchedule;
import com.codestack.trippooling.mongo.document.User;
import com.codestack.trippooling.service.UserService;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = TripPoolingApplication.class)
@WebMvcTest(UserController.class)
public class UserControllerTest {

	@Autowired
	private MockMvc mvc;

	@MockBean
	private UserService userService;

	private User firstUser;
	private User secondUser;
	private List<User> users;
	private TripDetails tripDetails;
	private Address destinationAddress;
	private Address originAddress;
	private TripSchedule tripSchedule;

	@BeforeEach
	public void setUp() {
		setUpUsers();
	}

	private void setUpUsers() {
		setUpRouteDetails();
		firstUser = User.builder().id("abc").firstName("Jan").lastName("Kowalski").phoneNumber("123123")
				.tripDetails(tripDetails).build();
		secondUser = User.builder().id("zxc").firstName("Karol").lastName("Okrasa").phoneNumber("54321")
				.tripDetails(tripDetails).build();
		users = Arrays.asList(firstUser, secondUser);
	}

	private void setUpRouteDetails() {
		setUpAddresses();
		setUpRouteSchedule();
		tripDetails = TripDetails.builder().originAddress(originAddress).destinationAddress(destinationAddress)
				.tripSchedule(tripSchedule).build();

	}

	private void setUpAddresses() {
		destinationAddress = Address.builder().description("Destination address")
				.point(new GeoJsonPoint(54.372158, 18.638306)).build();
		originAddress = Address.builder().description("Origin address").point(new GeoJsonPoint(54.156059, 19.404490))
				.build();
		;

	}

	private void setUpRouteSchedule() {
		tripSchedule = TripSchedule.builder()
				.dayOfWeeks(new HashSet<>(Arrays.asList(DayOfWeek.MONDAY, DayOfWeek.SATURDAY)))
				.departureTime(LocalTime.of(6, 30)).returnTime(LocalTime.of(16, 30)).build();

	}

	@Test
	public void createTest() throws Exception {
		when(userService.findAll()).thenReturn(users);
		mvc.perform(get("/user")).andExpect(status().isOk()).andExpect(jsonPath("$", hasSize(2)))
				.andExpect(jsonPath("$[0].firstName", is(firstUser.getFirstName())));
	}

}
