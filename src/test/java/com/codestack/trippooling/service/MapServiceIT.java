package com.codestack.trippooling.service;

import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.geo.Point;

import com.google.maps.errors.ApiException;
import com.google.maps.model.DirectionsResult;
import com.google.maps.model.DistanceMatrix;

@SpringBootTest
public class MapServiceIT {

	@Autowired
	private MapService mapService;
	private Point originPoint;
	private Point destinationPoint;

	@BeforeEach
	public void setUp() {
		originPoint = new Point(54.52347261, 18.52981567);
		destinationPoint = new Point(54.40834112, 18.58903885);
	}

	@Test
	public void findDirectionTest() throws ApiException, InterruptedException, IOException {
		DirectionsResult result = mapService.findDirection(originPoint, destinationPoint);
		assertTrue(result.routes.length > 0);
	}

	@Test
	public void findDistanceMatrixTest() throws ApiException, InterruptedException, IOException {
		DistanceMatrix result = mapService.findDistanceMatrix(originPoint, destinationPoint);
		assertTrue(result.rows.length > 0);
	}
}
