package com.codestack.trippooling.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.codestack.trippooling.model.RouteDetails;
import com.google.maps.GeoApiContext;
import com.google.maps.model.Distance;
import com.google.maps.model.DistanceMatrix;
import com.google.maps.model.DistanceMatrixElement;
import com.google.maps.model.DistanceMatrixRow;
import com.google.maps.model.Duration;

@ExtendWith(SpringExtension.class)
public class MapServiceTest {

	private MapService mapService;
	@Mock
	private GeoApiContext geoApiContext;
	private DistanceMatrix distanceMatrix;
	private DistanceMatrixRow[] rows;
	private DistanceMatrixRow row;
	private DistanceMatrixElement[] elements;
	private List<RouteDetails> routeDetailsList;
	private Distance distance1;
	private Distance distance2;
	private Duration duration1;
	private Duration duration2;

	@BeforeEach
	public void setUp() {
		mapService = new MapService(geoApiContext);
		setUpDistancesAndDurations();
		setUpDistanceMatrix();
		setUpRouteDetailsList();
	}

	private void setUpDistancesAndDurations() {
		distance1 = new Distance();
		distance1.inMeters = 10000l;
		distance1.humanReadable = "10 km";
		duration1 = new Duration();
		duration1.inSeconds = 14000l;
		duration1.humanReadable = "14000 sec";
		distance2 = new Distance();
		distance2.inMeters = 15000l;
		distance2.humanReadable = "12 km";
		duration2 = new Duration();
		duration2.inSeconds = 18000l;
		duration2.humanReadable = "18000 sec";
	}

	private void setUpDistanceMatrix() {
		setUpRow();
		rows = new DistanceMatrixRow[] { row };
		distanceMatrix = new DistanceMatrix(new String[] { "Gdynia" }, new String[] { "Sopot", "Gdansk" }, rows);
	}

	private void setUpRow() {
		row = new DistanceMatrixRow();
		setUpElements();
		row.elements = elements;
	}

	private void setUpElements() {
		DistanceMatrixElement el1 = new DistanceMatrixElement();
		el1.distance = distance1;
		el1.duration = duration1;
		DistanceMatrixElement el2 = new DistanceMatrixElement();
		el2.distance = distance2;
		el2.duration = duration2;
		elements = new DistanceMatrixElement[] { el1, el2 };
	}

	private void setUpRouteDetailsList() {
		RouteDetails routeDetails1 = RouteDetails.builder("Gdynia", "Sopot").distance(distance1).duration(duration1)
				.build();
		RouteDetails routeDetails2 = RouteDetails.builder("Gdynia", "Gdansk").distance(distance2).duration(duration2)
				.build();
		routeDetailsList = Arrays.asList(routeDetails1, routeDetails2);
	}

	@Test
	public void parseDistanceMatrixTest() {
		List<RouteDetails> actualList = mapService.parseDistanceMatrix(distanceMatrix);
		assertEquals(routeDetailsList, actualList);
	}

}
