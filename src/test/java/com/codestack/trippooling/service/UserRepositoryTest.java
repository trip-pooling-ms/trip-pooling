package com.codestack.trippooling.service;

import static org.junit.Assert.assertTrue;

import java.time.DayOfWeek;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.data.geo.Distance;
import org.springframework.data.geo.Metrics;
import org.springframework.data.geo.Point;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;

import com.codestack.trippooling.mongo.document.Address;
import com.codestack.trippooling.mongo.document.TripDetails;
import com.codestack.trippooling.mongo.document.TripSchedule;
import com.codestack.trippooling.mongo.document.User;
import com.codestack.trippooling.mongo.repository.UserRepository;

@DataMongoTest
public class UserRepositoryTest {

	private User firstUser;
	private User secondUser;
	private TripDetails tripDetails1;
	private TripDetails tripDetails2;
	private Address destinationAddress;
	private Address originAddress1;
	private Address originAddress2;
	private TripSchedule tripSchedule;

	@Autowired
	private UserRepository userRepository;

	@BeforeEach
	public void setUp() {
		setUpUsers();
		userRepository.save(firstUser);
		userRepository.save(secondUser);
	}

	private void setUpUsers() {
		setUpRouteDetails();
		firstUser = User.builder().id("abc").firstName("Jan").lastName("Kowalski").phoneNumber("123123")
				.tripDetails(tripDetails1).build();
		secondUser = User.builder().id("zxc").firstName("Karol").lastName("Okrasa").phoneNumber("54321")
				.tripDetails(tripDetails2).build();
	}

	private void setUpRouteDetails() {
		setUpAddresses();
		setUpRouteSchedule();
		tripDetails1 = TripDetails.builder().originAddress(originAddress1).destinationAddress(destinationAddress)
				.tripSchedule(tripSchedule).build();
		tripDetails2 = TripDetails.builder().originAddress(originAddress2).destinationAddress(destinationAddress)
				.tripSchedule(tripSchedule).build();

	}

	private void setUpAddresses() {
		destinationAddress = Address.builder().description("Destination address")
				.point(new GeoJsonPoint(54.40834112, 18.58903885)).build();
		originAddress1 = Address.builder().description("Gdynia").point(new GeoJsonPoint(54.52347261, 18.52981567))
				.build();
		originAddress2 = Address.builder().description("Przymorze").point(new GeoJsonPoint(54.40834112, 18.58903885))
				.build();
		;

	}

	private void setUpRouteSchedule() {
		tripSchedule = TripSchedule.builder()
				.dayOfWeeks(new HashSet<>(Arrays.asList(DayOfWeek.MONDAY, DayOfWeek.SATURDAY)))
				.departureTime(LocalTime.of(6, 30)).returnTime(LocalTime.of(16, 30)).build();

	}

	@Test
	public void findByRouteDetails_OriginAddress_PointNearTest() {
		List<User> users = userRepository.findByRouteDetailsOriginAddressPointNear(new Point(54.40874075, 18.58680725), // Przymorze
				new Distance(15, Metrics.KILOMETERS));
		System.out.println("users: " + users);
		assertTrue(users != null);
	}


}
